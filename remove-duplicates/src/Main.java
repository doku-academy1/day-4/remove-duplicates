import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer[] arrays = {2, 3, 3, 4, 6, 9, 9};
        List<Integer> convertToList = convertArrayToList(arrays);

        Set<Integer> listResult = new LinkedHashSet<>();
        for (int i = 0; i<convertToList.size(); i++) {
            listResult.add(convertToList.get(i));
        }
        System.out.println(listResult.size());
    }

    public static List<Integer> convertArrayToList(Integer[] arrays) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i<arrays.length; i++) {
            list.add(arrays[i]);
        }
        return list;
    }
}